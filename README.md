## CiscoHab

This tool allows to browse [OpenHAB sitemaps](https://www.openhab.org/docs/configuration/sitemaps.html) 
from Cisco 79xx IP phones. Build with Ruby 2.7.2 and Sinatra.

Available [as a Docker container](https://hub.docker.com/r/duhast/ciscohab) as well.

**Launch**

Assuming Ruby 2.7.2+ is installed, run `bundle install` once and launch OpenHab with a single command:
```
rackup -o 0.0.0.0
```
If you need CiscoHab listen on a single interface, change `0.0.0.0` to it's IP address. Default port number
is `9292`, to override t, use the `-p` option (i.e. `-p 4545`)

Launching with Docker in background is as simple as:
```bash
docker run -d --name ciscohab -p 8081:9292 \
  -e CISCOHAB_URL=http://192.168.0.10:8081 \
  -e OPENHAB_URL=http://192.168.0.14:11 \
  duhast/ciscohab
``` 
Make sure to change IPs and mapped port to yours in the above command. 

**Configuration**

Configuration is performed using the following environment variables:
* `OPENHAB_URL` _(required)_ - URL to your OpenHAB service, i.e. http
* `CISCOHAB_URL` _(required)_ - URL of how your CiscoHab instance is accessible, i.e. http
* `CISCOHAB_LOCALE` _(optional)_ - Language setting (`en` [default], `ru`, `ua`)
* `EXT_DIRECTORY_URL` _(optional)_ - URL of your directory XML (``) to be converted to phone menu.
This is convenient helper for phones which does not support directoy XML, lica 7911G.
 

**Usage**

URLs listed below are available. You can set them as `<informationURL>` or `<servicesURL>` node value in your Cisco's 
_SEP*.cnf.xml_ config files.

* `/sites` - lists all OpenHAB sitemap links
* `/page/<site_name>/<page_id>` - particular section of chosen sitemap (get exact links from the sitemap list URL)
* `/phonebook` - your directory XML converted to phone menu (given than `EXT_DIRECTORY_URL` env var is set properly)

or just open root path to see what's available.