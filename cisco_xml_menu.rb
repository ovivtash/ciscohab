class CiscoXmlMenu

  def self.build
    raise ArgumentError, 'block missing' unless block_given?
    menu = self.new
    yield menu
    menu.to_xml
  end

  def initialize
    @doc = Ox::Document.new

    instruct = Ox::Instruct.new(:xml)
    instruct[:version] = '1.0'
    instruct[:encoding] = 'UTF-8'
    instruct[:standalone] = 'yes'
    @doc << instruct

    @root = Ox::Element.new('CiscoIPPhoneMenu')
    @doc << @root
  end

  def to_xml
    Ox.dump(@doc)
  end

  def set_title(title)
    @title_node ||= Ox::Element.new('Title').tap{|n| @root << n }
    @title_node << title
  end

  def new_item(item_name, item_url)
    item = Ox::Element.new('MenuItem')
    name = Ox::Element.new('Name')
    name << item_name
    item << name
    url = Ox::Element.new('URL')
    url << item_url
    item << url
    item
  end

  def add_dial(name, telephone)
    @root << new_item(name, "Dial:#{telephone}")
  end

  def add_url(name, url)
    @root << new_item(name, url)
  end

end