class CiscoXmlText

  def self.build
    raise ArgumentError, 'block missing' unless block_given?
    menu = self.new
    yield menu
    menu.to_xml
  end

  def initialize
    @doc = Ox::Document.new

    instruct = Ox::Instruct.new(:xml)
    instruct[:version] = '1.0'
    instruct[:encoding] = 'UTF-8'
    instruct[:standalone] = 'yes'
    @doc << instruct

    @root = Ox::Element.new('CiscoIPPhoneText')
    @doc << @root

    @last_softkey = 0
  end

  def to_xml
    Ox.dump(@doc)
  end

  def set_title(title)
    @title_node ||= Ox::Element.new('Title').tap{|n| @root << n }
    @title_node << title
  end

  def set_prompt(prompt)
    @prompt_node ||= Ox::Element.new('Prompt').tap{|n| @root << n }
    @prompt_node << prompt
  end

  def add_text(text)
    node = Ox::Element.new('Text')
    node << text
    @root << node
  end

  def add_softkey(key_name, key_url)
    @root << Ox::Element.new('SoftKeyItem').tap do |item|
      item << Ox::Element.new('Name').tap{|n| n << key_name}
      item << Ox::Element.new('URL').tap{|n| n << key_url}
      item << Ox::Element.new('Position').tap{|n| n << (@last_softkey += 1).to_s}
    end
  end

  def add_default_softkey(key_name)
    add_softkey(key_name, "SoftKey:#{key_name}")
  end



end