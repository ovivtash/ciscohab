FROM ruby:2.7.2-slim
MAINTAINER Oleg Vivtash <o@vivtash.net>

RUN apt-get update && apt-get install -y build-essential
COPY . /app
WORKDIR /app
RUN bundle install

EXPOSE 9292
CMD ["rackup", "-o", "0.0.0.0"]