require 'rubygems'
require 'bundler/setup'
Bundler.require
require './cisco_xml_menu'
require './cisco_xml_text'

I18n.load_path << Dir[File.join(File.expand_path('locales'), '*.yml')]
I18n.default_locale = ENV.fetch('CISCOHAB_LOCALE', 'en').to_sym

set(:ciscohab_url){ ENV.fetch('CISCOHAB_URL') }
set(:openhab_root_url){ ENV.fetch('OPENHAB_URL') }
set(:cisco_directory_url){ ENV.fetch('EXT_DIRECTORY_URL') }
set(:show_exceptions){ ENV.has_key?('DEV_MODE') }

item_names = {}

class OpenHabError < StandardError
  attr_reader :response, :path

  def initialize(path, error_response)
    @path = path
    @error_response = error_response
    msg = error_response['error']['message']
    super(msg)
  end
end

error OpenHabError do
  content_type 'text/plain'
  status 400
  e = env['sinatra.error']
  ["OpenHAB Error:", e.message, e.path].join("\n")
end

error do
  content_type 'text/plain'
  status 500
  e = env['sinatra.error']
  ["CiscoHab Error: #{e.class.name}", e.message, e.backtrace.first.split('/').last].join("\n")
end

helpers do
  def get_json(url)
    resp = HTTParty.get(url, follow_redirects: true)
    Oj.load(resp.body).tap do |json|
      if json.is_a?(Hash) && json.has_key?('error')
        path = url.split('/')[-2..-1].unshift('OHAPI').join('/')
        raise OpenHabError.new(path, json)
      end
    end
  end

  def format_state(state)
    case state
      when 'UNDEF'
        I18n.t 'state.undef'
      when 'NULL'
        I18n.t 'state.null'
      when /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/
        DateTime.parse(state).strftime(I18n.t('state.datetime'))
      else
        state
    end
  end
end

before do
  content_type 'text/xml'
end

get '/' do
  content_type 'text/html'
  erb :about
end

get '/phonebook' do
  pb_resp = HTTParty.get(settings.cisco_directory_url, follow_redirects: true)
  dir_doc = Ox.parse(pb_resp.body)
  CiscoXmlMenu.build do |menu|
    menu.set_title I18n.t('title.contacts')
    dir_doc.locate('CiscoIPPhoneDirectory/DirectoryEntry').each do |entry|
      contact_name = entry.locate('Name').first&.text
      contact_phone = entry.locate('Telephone').first&.text
      menu.add_dial contact_name, contact_phone
    end
  end
end


get '/sites' do
  sites = get_json "#{settings.openhab_root_url}/rest/sitemaps"
  CiscoXmlMenu.build do |menu|
    menu.set_title I18n.t('title.sites')
    sites.each do |site|
      menu.add_url site['label'], [settings.ciscohab_url, 'page', site['name'], site['name']].join('/')
    end
  end
end

get '/page/*' do |path|
  site_name, _ = path.split('/', 2)
  data = get_json "#{settings.openhab_root_url}/rest/sitemaps/#{path}"
  CiscoXmlMenu.build do |menu|
    menu.set_title data['title']
    data['widgets'].each do |widget|
      if widget.has_key?('linkedPage')
        menu.add_url widget['label'], [settings.ciscohab_url, 'page', site_name, widget['linkedPage']['id']].join('/')
      elsif widget.has_key?('item')
        item_name =  widget['item']['name']
        item_label = widget['label']
        item_label = $1 if item_label =~ /(.+)\s+\[.+\]/
        item_names[item_name] = item_label
        menu.add_url widget['label'], [settings.ciscohab_url, 'item', item_name].join('/')
      else
        menu.add_url widget['label'], [settings.ciscohab_url, 'page', site_name, widget['widgetId']].join('/')
      end
    end
  end
end

get '/item/:item_id' do
  data = get_json "#{settings.openhab_root_url}/rest/items/#{params[:item_id]}"
  CiscoXmlText.build do |text|
    item_name = data['name']
    text.set_title item_names.fetch(item_name, item_name)
    text.add_text format_state(data['state'])
  end
end